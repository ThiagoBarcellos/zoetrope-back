const { response } = require('express');
const Post = require('../models/Post');
const User = require('../models/User');

const index = async (req, res) => {
    try {
        const posts = await Post.findAll();
        return res.status(200).json({ posts });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const post = await Post.findByPk(id);
        return res.status(200).json({ post });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const create = async (req, res) => {
    try {
        const newPost = {
            text: req.body.text,
            attachment: req.body.attachment
        }
        const post = await Post.create(newPost);
        return res.status(201).json({ message: "Post cadastrado com sucesso!", post: post });
    } catch (err) {
        res.status(500).json({ error: err });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    try {
        const [updated] = await Post.update(req.body, { where: { id: id } });
        if (updated) {
            const post = await Post.findByPk(id);
            return res.status(200).send(post);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Post não encontrado");
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await Post.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Post deletado com sucesso.");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Post não encontrado.");
    }
};

const addUser = async (req, res) => {
    const { id } = req.params;
    try {
        const post = await Post.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await post.setUser(user);
        return res.status(200).json(post);
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const removeUser = async (req, res) => {
    const { id } = req.params;
    try {
        const post = await Post.findByPk(id);
        await post.setUser(null);
        return res.status(200).json(post);
    } catch (err) {
        return res.status(500).json({ err });
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addUser,
    removeUser
};