const { response } = require('express');
const User = require('../models/User');

const index = async (req, res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({ users });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({ user });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const create = async (req, res) => {
    try {
        const newUser = {
            name: req.body.name,
            email: req.body.email,
            birth_date: req.body.birth_date,
            gender: req.body.gender,
            favorite_show: req.body.favorite_show,
            favorite_film: req.body.favorite_film
        }
        const user = await User.create(newUser);
        return res.status(201).json({ message: "Usuário cadastrado com sucesso!", user: user });
    } catch (err) {
        res.status(500).json({ error: err });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    try {
        const [updated] = await User.update(req.body, { where: { id: id } });
        if (updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};