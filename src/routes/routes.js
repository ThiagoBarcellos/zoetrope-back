const { Router } = require('express');
const UserController = require('../controllers/UserController');
const PostController = require('../controllers/PostController');
const CommentController = require('../controllers/CommentController');
const router = Router();

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

router.get('/posts', PostController.index);
router.get('/posts/:id', PostController.show);
router.post('/posts', PostController.create);
router.put('/posts/:id', PostController.update);
router.delete('/posts/:id', PostController.destroy);
router.delete('/postsadduser/:id', PostController.addUser);
router.delete('/postsremoveuser/:id', PostController.removeUser);

router.get('/comments', CommentController.index);
router.get('/comments/:id', CommentController.show);
router.post('/comments', CommentController.create);
router.put('/comments/:id', CommentController.update);
router.delete('/comments/:id', CommentController.destroy);
router.delete('/commentsaddpost/:id', CommentController.addPost);
router.delete('/commentsremovepost/:id', CommentController.removePost);

module.exports = router;