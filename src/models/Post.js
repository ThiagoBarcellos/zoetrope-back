const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Post = sequelize.define('Post', {
  author: {
    type: DataTypes.STRING,
    allowNull: false
  },
  
  like_number: {
    type: DataTypes.NUMBER
  },
  
  comment_number: {
    type: DataTypes.NUMBER
  },
  
  share_number: {
    type: DataTypes.NUMBER
  },
  
  text: {
    type: DataTypes.STRING,
    allowNull: false
  },

  attachment: {
    type: DataTypes.STRING
  }

}, {
  //timestamps: false
});

Post.associate = function (models) {
  Post.belongsTo(models.User, {});
  Post.hasMany(models.Comment, {});
}

module.exports = Post;