const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    birth_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    gender: {
        type: DataTypes.STRING
    },

    favorite_show: {
        type: DataTypes.STRING
    },

    favorite_film: {
        type: DataTypes.STRING
    },

}, {
    //timestamps: false
});

User.associate = function (models) {
    User.hasMany(models.Post, {});
    //User.belongsTo(models.Role, { });
}

module.exports = User;