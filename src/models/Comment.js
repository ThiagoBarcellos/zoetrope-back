const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Comment = sequelize.define('Comment', {
  text: {
    type: DataTypes.STRING,
    allowNull: false
  },

  like_number: {
    type: DataTypes.NUMBER
  },

  comment_number: {
    type: DataTypes.NUMBER
  },

}, {
  //timestamps: false
});

Comment.associate = function (models) {
  Comment.belongsTo(models.Post, {});
}

module.exports = Comment;